function soko() {
	var room, roomArray, player, width, boxLeft, boxContent, boxMessage;
	function init() {
		room = '#######'+
			   '#@....#'+
			   '#.#O..#'+
			   '#..O.##'+
			   '#.#.O..'+
			   '#.OO#.#'+
			   '#.O.O.#'+
			   '#.....#'+
			   '###+###';

		roomArray = room.split(''); // добавляем карту в массив
		player = 8; // начальный индекс позиции игрока (0 - верхний левый угол карты, 62 - правый нижний)
		width = 7; // ширина игрового поля (в символах)
		boxLeft = 7; // число ящиков в игре
		boxContent = [ // содержимое ящиков для boxMessage
			'мусором',
			'медикаментами',
			'оружием',
			'продовольствием',
			'планами колонизации',
			'подарками для аборигенов',
			'инструментами'
		];
		boxMessage = 'Избавься от всех контейнеров!'; // сообщение об отправке ящиков за борт
		render(); // рисуем экран
	};
	init(); // устанавливаем стартовые значения

	// отрисовка экрана
	function render() { 
		$('#field').html(''); // очищаем экран
		for (var i = 0; i < roomArray.length + 1; i++) { // рисуем карту посимвольно
			$('#field').append(roomArray[i]);
			((i + 1) % width == 0) && $('#field').append('<br>'); // после каждой i кратной width делаем перенос на следующую строку 
		};
		$('#field').append('<p style = "letter-spacing: normal;"><br>Осталось контейнеров: ' + boxLeft + '<br>' + boxMessage); // добавляем инфо сообщения
		story.statusScreen(); // обновляем счет
	};
	
	// обработчики нажатия кнопок
	document.onkeydown = function checkKeycode(event) {
		// кнопка D (вправо)
		(event.keyCode === 68) && $("#buttonRight").click();
		// кнопка S (влево)
		(event.keyCode === 65) && $("#buttonLeft").click();
		// кнопка W (вверх)
		(event.keyCode === 87) && $("#buttonUp").click();
		// кнопка S (вниз)
		(event.keyCode === 83) && $("#buttonDown").click();
		// кнопка X (перезапуск)
		(event.keyCode === 88) && $("#buttonRestart").click();
	};
		
	// обработчики клика по виртуальным кнопкам
	// кнопка "Вправо"
	$('#buttonRight').click(function() {playerMove(1, 2); return false;});
	// кнопка "Влево"
	$('#buttonLeft').click(function() {playerMove(-1, -2); return false;});
	// кнопка "Вверх"
	$('#buttonUp').click(function() {playerMove(-width, -2 * width); return false;});
	// кнопка "Вниз"
	$('#buttonDown').click(function() {playerMove(width, 2 * width); return false;});
	// кнопка "Заново"
	$('#buttonRestart').click(function() {restart();});

	/* движение персонажа и объектов
	   distance1 - соседняя клетка с @
	   distance2 - клетка через одну от @
	*/ 
	function playerMove(distance1, distance2) {
		// движение ящика
		if (roomArray[player + distance1] == 'O' && roomArray[player + distance2] == '.') { // если есть куда толкать ящик
			roomArray[player + distance2] = 'O'; // перемещаем ящик
			roomArray[player + distance1] = '.'; // убираем ящик с текущей клетки
		}
		
		// попытка открыть дверь
		if (roomArray[player + distance1] == '+' && boxLeft > 0) {
			boxMessage = 'Ты еще не избавился от всех контейнеров'
		};
		
		// движение игрока
		if (roomArray[player + distance1] == '.') { // если соседняя клетка свободна
			roomArray[player] = '.'; // текущая клетка становится свободной
			player += distance1; // переносим игрока на новую клетку
		}
		
		
		// попытка войти в шлюз
		if (player == 34) {
			player = 33;
			boxMessage = 'Становиться искусственным спутником не входит в твои планы'
		};

		roomArray[player] = '@'; // обновляем текущую клетку с игроком
		// если сбросил ящик в шлюз
		if (roomArray[34] == 'O') { // ящик в шлюзе
			roomArray[34] = '.'; // убираем ящик
			boxLeft--; // уменьшаем число ящиков
			story.score += 5; // увеличиваем счет
			boxMessage = 'Контейнер с ' + boxContent.pop() + ' отправился за борт!'; //сообщение для вывода
			(boxContent.length == 2) && (boxMessage += ' Прощай, оружие!'); // пасхалка
		}
		// если ящиков не осталось, закрываем шлюз, открываем выход, и переходим на экран "конец"
		if (boxLeft == 0) {
			roomArray[34] = '+';
			roomArray[59] = '.';
			boxMessage = 'Ты справился! Возвращайся на мостик';
		};
		(player == 59) && story.turnTo('конец');
		render();
	};
	
	// перезапуск головоломки
	function restart() {
		// если еще остались ящики
		if (boxLeft > 0) { 
			var confirmRestart = confirm('Начать заново?');
			if (confirmRestart) {
				story.score -= ((7 - boxLeft) * 5) + 5; // минус заработанные за выброшенные ящики очки и 5 очков штрафа
				init();
			};
		};
	};
};  
  


	


	
